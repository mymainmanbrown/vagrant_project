# Load config JSON.
settings = JSON.parse(File.read("vagrant.settings.json"))

# @todo
# Ensure the settings json file has all required fields before proceeding

Vagrant.configure("2") do |config|
  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = settings["base"]

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  if settings["use_private_ip"] && settings["private_ip"]
    config.vm.network :private_network, ip: settings["private_ip"]
  end

  # Create a public network which will allow your vagrant box to be accessible
  # over the network and potentially over the internet
  if settings["use_public_http"] || settings["use_public_https"]
    config.vm.network "public_network", bridge: settings["network_interface"]
  end
  if settings["use_public_http"] && settings["public_http_port"] && settings["public_http_port"] > 0
    config.vm.network "forwarded_port", guest: 80, host: settings["public_http_port"]
  end
  if settings["use_public_https"] && settings["public_https_port"] && settings["public_https_port"] > 0
    config.vm.network "forwarded_port", guest: 443, host: settings["public_https_port"]
  end
  if settings["forwarded_ports"]
    settings["forwarded_ports"].each do |port|
      config.vm.network "forwarded_port", 
        guest: port["guest"], 
        host: port["host"],
        host_ip: port["host_ip"],
        id: port["id"],
        auto_correct: port["auto_correct"]
    end
  end

  config.vm.provider :virtualbox do |v|
    # v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--memory", settings["memory"]]
    v.customize ["modifyvm", :id, "--name", settings["name"]]
    v.gui = settings["gui"]
  end

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  #
  # @see http://jeremykendall.net/2013/08/09/vagrant-synced-folders-permissions/
  if settings["synced_folders"]
    settings["synced_folders"].each do |folder|
      config.vm.synced_folder folder["host_path"], folder["guest_path"],
        id: folder["id"],
        owner: folder["owner"],
        group: folder["group"],
        mount_options: [folder["mount_options"]]
    end
  end

  # Sync folders AFTER provisioning has taken place.  Useful for syncing folders
  # with user permissions other than vagrant.
  # https://github.com/mitchellh/vagrant/issues/936#issuecomment-7179034
  if settings["deferred_synced_folders"]
    settings["deferred_synced_folders"].each do |folder|
      # Mount it first with vagrant permissions
      config.vm.synced_folder folder["host_path"], folder["guest_path"],
        id: folder["id"],
        owner: "vagrant",
        group: "vagrant"
      # Remount it later with the proper permissions
      config.trigger.after :up do
        run "sshpass -p vagrant ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no vagrant@" + settings["ip"] + " 'sudo mount -t vboxsf -o uid=`id -u " + folder["owner"] + "`,gid=`id -g " + folder["group"] + "` " + folder["host_path"] + " " + folder["guest_path"] + "'"
      end
    end
  end

  # Run the host scripts at the beginning of vagrant up
  if settings['host_pre_up']
    config.trigger.before :up do
      settings["host_pre_up"].each do |script|
        system(File.read(script["dir"] + '/' + script["box"] + '/' + script["name"] + '.' + script["ext"]))
      end
    end
  end

  # @todo
  # derive the box name from the config.vm.box name
  # check to confirm the directory exists
  # confirm the file exists
  # fail on any error
  if settings['guest_provision']
    settings["guest_provision"].each do |script|
      # Sane defaults
      if ! script["dir"]
        script["dir"] = "provision"
      end
      if ! script["ext"]
        script["ext"] = "sh"
      end
      if ! script["box"] && settings["base_short"]
        script["box"] = settings["base_short"]
      end
      config.vm.provision :shell, :path => script["dir"] + '/' + script["box"] + '/' + script["name"] + '.' + script["ext"]
    end
  end

  # Run post-up scripts
  if settings["guest_post_up"]
    settings["guest_post_up"].each do |script|
      config.trigger.after :up do
        run "sshpass -p vagrant ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no vagrant@" + settings["ip"] + " '\n" + File.read(script["dir"] + '/' + script["box"] + '/' + script["name"] + '.' + script["ext"]) + "\n'"
      end
    end
  end

end
