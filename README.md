Vagrant Base for PHP-based web projects
===

I created this sub-project out of a need to have a web server that I could quickly
reproduce as many times as I needed.  In theory, one for each project.  I considered
creating images or using virtualbox snapshots, but those are not easily shareable,
even if I created a fully automated installation script.  I created my reproducible
web server as a vagrant box as it provides the added benefits of easy modification,
easy sharing, and relatively lightweight configuration, which has an additional
benefit of being easily created as a git project.

Additionally, by creating all of my provisions as basic shell scripts (rather than
using things like puppet, chef, ansible, or salt), they can be easily deployed to
another server, and you have direct access to the shell commands.  This way, if you
decide to use some other vagrant box (like laravel homestead) or an automation
technology (chef, ansible, etc.), the provision scripts in this project can still
be modified and used with those vagrant boxes.

Assumptions
---
When using this project, here are the assumptions I've made:

1. You will always have at least the default synced folder ('./shared' -> '/home/vagrant/host'), though you may add more
1. You will always run at least the "bootstrap" provision


Web Server Components
---

While any number of programs, tools, or utilities can be used to create a web server,
I am most in need of a web server that has the following components:

1. PHP > 5.3
1. MySQL
1. Apache2
1. phpmyadmin (luxury)

However, the actual vagrant configuration I use is not dependent on any of these things.
If you have different requirements for your web server (such as the standard ubuntu PHP
version of 5.3), just modify the provision files as necessary and then add the file
names in the vagrant.json file and you can still use this Vagrantfile for your vagrant
box configuration.


Reason for creating this project
---

At work, the benefit of using Vagrant became apparent the first time I used it.  I now
had the exact same server development environment as everyone else, it was set up by
our server adminstrator, so was properly configured and tuned and had all dependencies
preinstalled, and it was trivial to set up on multiple machines.  I decided I needed to
learn how to set one up for myself, and thus this project was born.

I decided to put it in a public repo because I had more trouble with it than I had expected.
I actually didn't have any problem with vagrant or with the sh file I created for
provisioning (though debconf did take a fair amount of research).  Rather, the problems
I had arose from attempting to use chef-solo and the associated [opscode](http://community.opscode.com/cookbooks)
cookbooks and recipes.  For me, they were riddled with depencies (which I thought I had
managed to resolve) and other errors that were not apparent and not easy to find relevant
answers for.  Since I have projects to complete, I decided not to spend any more time
attempting to research the problems I was having with the most basic of installs (apache2
and mysql) and just write the provisioning logic myself.

As such, I thought perhaps what I have written may help you get your feet wet with vagrant
and get your projects off the ground more quickly.  Eventually, it will be beneficial to
learn how to set up Vagrant with the something like ansible or chef, but for now, I have been
able to create the environment I need without them.
