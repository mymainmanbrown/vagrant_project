#!/usr/bin/env bash

# Laravel requires mcrypt
apt-get install -y php5-mcrypt
apt-get install -y php5-curl
apt-get install -y curl

# Install composer globally
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
chmod +x /usr/local/bin/composer

# Install laravel globally
curl -o /usr/local/bin/laravel http://laravel.com/laravel.phar
chmod +x /usr/local/bin/laravel

# Get rid of the site directory
rm -rf /home/vagrant/host/laravel/*
rm -rf /home/vagrant/host/laravel/.*

# Install laravel in the site directory
cd /home/vagrant/host
laravel new laravel

# Create the laravel public folder as the root site directory
unlink /var/laravel
ln -fs /home/vagrant/host/laravel/public /var/laravel

# Laravel has a recommended method of storing sensitive data
# http://laravel.com/docs/configuration#protecting-sensitive-configuration
# The laravel project root already has a .gitignore file that will ignore this
touch /home/vagrant/host/laravel/.env.php
