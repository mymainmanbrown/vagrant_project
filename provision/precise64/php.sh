apt-get install -y php5 libapache2-mod-php5 php5-mcrypt php5-gd php5-curl

# Display errors
sed -i.bak "s/display_errors = Off/display_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/display_startup_errors = Off/display_startup_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/post_max_size = 8M/post_max_size = 1024M/" /etc/php5/apache2/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 1024M/" /etc/php5/apache2/php.ini
