#!/usr/bin/env bash

# Install Postgresql and the php extension for it
# https://help.ubuntu.com/community/PostgreSQL
apt-get install -y php5-pgsql postgresql postgresql-contrib

# Create the psql user & db
# http://www.postgresql.org/docs/9.1/interactive/sql-createrole.html
# http://stackoverflow.com/questions/18345107/automating-install-and-creation-of-postgresql-databases-in-shell
echo "CREATE USER root WITH PASSWORD 'root';" | sudo -u postgres psql
echo "ALTER USER root CREATEDB;" | sudo -u postgres psql

# Create the pgsql password file for unrestricted local access for the root
# user.  This is mainly needed for the drush site-install command to work
# without prompts.
# http://drupal.stackexchange.com/questions/35198/drush-sql-dump-asking-for-password
# http://www.postgresql.org/docs/9.1/static/libpq-pgpass.html
echo "*:*:*:root:root" > /home/vagrant/.pgpass
chown vagrant:vagrant /home/vagrant/.pgpass
chmod 0600 /home/vagrant/.pgpass

service postgresql reload

# Create the DB with the proper & expected character encoding settings
# echo "CREATE DATABASE mydb WITH ENCODING='UTF8' LC_CTYPE='en_US.UTF-8' LC_COLLATE='en_US.UTF-8' OWNER=root TEMPLATE=template0 CONNECTION LIMIT=-1;" | sudo -u postgres psql

