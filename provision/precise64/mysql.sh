#!/usr/bin/env bash

# Install MySQL
# This one took quite a bit more research.  See this
# resource for a description of each line.
# http://www.microhowto.info/howto/perform_an_unattended_installation_of_a_debian_package.html
export DEBIAN_FRONTEND=noninteractive
echo mysql-server-5.5 mysql-server/root_password password | debconf-set-selections
echo mysql-server-5.5 mysql-server/root_password_again password | debconf-set-selections
apt-get install -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" mysql-server

apt-get install -y libapache2-mod-auth-mysql php5-mysql
