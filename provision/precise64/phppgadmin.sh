# Install phppgadmin
apt-get install -y phppgadmin

# Configure phpmyadmin
sed -i.bak "s/# allow from all/allow from all/" /etc/phppgadmin/apache.conf
sed -i "s/\$conf\['extra_login_security'\] = true;/\$conf\['extra_login_security'\] = false;/" /etc/phppgadmin/config.inc.php
