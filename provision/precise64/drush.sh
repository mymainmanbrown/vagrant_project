# Use pear to install a more recent version of drush
apt-get install -y php-pear
pear channel-discover pear.drush.org
pear install drush/drush
# Run drush once so that it initializes itself
drush
# Change the ownership of the config file, since it gets created as root
chown -R vagrant:vagrant ~/.drush
