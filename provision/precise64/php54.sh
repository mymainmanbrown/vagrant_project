#!/usr/bin/env bash

# Add the repository that will allow us to install php 5.4
# http://oliversmith.io/technology/2011/12/03/installing-php-5-4-in-ubuntu/
# https://launchpad.net/~ondrej/+archive/php5
add-apt-repository -y ppa:ondrej/php5-oldstable
apt-get update -q

# Install php 5.4
apt-get install -y php5 php5-curl php5-gd

# Display errors
sed -i.bak "s/display_errors = Off/display_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/display_startup_errors = Off/display_startup_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/post_max_size = 8M/post_max_size = 1024M/" /etc/php5/apache2/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 1024M/" /etc/php5/apache2/php.ini
