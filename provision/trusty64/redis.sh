#!/bin/bash

add-apt-repository -y ppa:rwky/redis
apt-get update
apt-get install -y redis-server