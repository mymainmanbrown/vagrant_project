#!/bin/bash

# Install node tools
npm install -g forever
npm install -g npmbox
npm install -g bower -s
npm install -g grunt-cli -s
npm install -g requirejs
npm install -g gulp