#!/bin/bash

# http://docs.mongodb.org/master/tutorial/install-mongodb-on-ubuntu/?_ga=1.57786282.1699658796.1427212868
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.0.list
apt-get update
apt-get install -y mongodb-org