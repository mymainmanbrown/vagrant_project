#!/bin/bash

# @todo - improve comments

# Start out by ensuring we're using the most up to date
# repositories
apt-get update -q

# In order to easily add a PPA, you'll need this package
# http://linuxg.net/how-to-fix-error-sudo-add-apt-repository-command-not-found/
# http://askubuntu.com/questions/38021/how-to-add-a-ppa-on-a-server
apt-get install -y software-properties-common
apt-get install -y python-software-properties

# A little later, in order to automate some of the installs,
# we'll need the debian install utils
apt-get install -y debconf-utils

# Install the text editor of choice
apt-get install -y vim

# Note that I had originally installed git here, but I now think git should be
# managed on the host machine rather than the guest.
# HOWEVER, there are still certain applications (such as bower) that require
# git, so it still needs to be installed.  I still do not recommend using git
# in the VM to manage your project, mostly because of the potential permissions
# problems that may occur
apt-get install git