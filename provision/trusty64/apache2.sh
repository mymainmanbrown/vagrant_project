#!/bin/bash

# Install Apache2 Web Server
apt-get install -y apache2

# Remove all of the default apache site files
rm -rf /var/www/html

# Create a symlink from our host project vagrant directory to
# apache's default site directory.  This will allow non-root,
# and therefore easier, modification of project files, while
# at the same time keeping them easily accessible on the host.
# @todo - use a subdirectory of the vagrant root, maybe call
# it site, and use that as the /var/www symlink
ln -fs /home/vagrant/host/www /var/www/html

# Enable mod_rewrite
a2enmod rewrite

# Enable htaccess files
# cp /etc/apache2/sites-available/default /etc/apache2/sites-available/default.bak
# awk '/AllowOverride None/{c+=1}{if(c==2){sub("AllowOverride None","AllowOverride All",$0)};print}' /etc/apache2/sites-available/default.bak > /etc/apache2/sites-available/default

# Restart apache to ensure our new configuration (specifically,
# the recognition of our symlink) works.
service apache2 restart
