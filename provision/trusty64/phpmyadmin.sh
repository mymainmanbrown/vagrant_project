#!/bin/bash

# Install phpmyadmin
# http://gercogandia.blogspot.com/2012/11/automatic-unattended-install-of.html
export DEBIAN_FRONTEND=noninteractive
echo phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2 | debconf-set-selections
echo phpmyadmin phpmyadmin/dbconfig-install boolean true | debconf-set-selections
echo phpmyadmin phpmyadmin/app-password-confirm password | debconf-set-selections
echo phpmyadmin phpmyadmin/mysql/admin-pass password | debconf-set-selections
echo phpmyadmin phpmyadmin/mysql/app-pass password | debconf-set-selections
apt-get install -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" phpmyadmin

# Configure phpmyadmin
# @todo - implement this in a way that doesn't involve the replacement
# of essentially the entire line.
# Use sed to specifically target the lines that contain the desired
# configuration options and simply uncomment or entirely replace them
sed -i.bak "s/\/\/ \$cfg\['Servers'\]\[\$i\]\['AllowNoPassword'\]/\$cfg\['Servers'\]\[\$i\]\['AllowNoPassword'\]/" /etc/phpmyadmin/config.inc.php
# @todo - even though this sed operation works, the configuration
# doesn't actually create an automatically filled in login page.
sed -i "s/\/\/\$cfg\['Servers'\]\[\$i\]\['auth_type'\] = 'cookie';/\$cfg\['Servers'\]\[\$i\]\['auth_type'\] = 'config';\n\$cfg\['Servers'\]\[\$i\]\['username'\] = 'root';\n\$cfg\['Servers'\]\[\$i\]\['password'\] = '';/" /etc/phpmyadmin/config.inc.php
