#!/bin/bash

# https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server
curl -sL https://deb.nodesource.com/setup | bash -
apt-get update
apt-get install -y build-essential nodejs